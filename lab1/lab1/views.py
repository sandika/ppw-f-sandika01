from django.shortcuts import render

def index(request):

	personal_data = {'nama1' : 'Sandika P. Putra', 'nama2' : 'Douglas R. Faisal', 'nama3' : 'Bricen S. Simamora'}
	return render(request, 'index.html', personal_data)